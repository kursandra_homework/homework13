import { expect, test } from '@jest/globals';
import { apiProvider } from '../framework';
import { email, diffEmails } from '../framework/builder/emails';
import { user } from '../framework/config';

jest.setTimeout(7000);
describe('Отправляем http', () => {
  test('положительный', async () => {
    const response = await apiProvider()
      .mailboxlayer()
      .checkEmail(email.positiveEmail, user.mailboxlayer.access_key);
    expect(response.body.email).toEqual(email.positiveEmail);
    expect(response.status).toEqual(200);
  });

  test.each(diffEmails)('параметризированный', async (mail, expected) => {
    const delay = async (ms) => await new Promise(resolve => setTimeout(resolve, ms));
    await delay(4000);
    const response = await apiProvider().mailboxlayer().checkEmail(mail, user.mailboxlayer.access_key);
    expect(response.status).toEqual(200);
    expect(response.body.format_valid).toEqual(expected);
  });

  test('проверка доступа', async () => {
    const r = await apiProvider().mailboxlayer().checkEmail(email.positiveEmail);
    expect(r.status).toEqual(200);
    expect(r.body.error.code).toEqual(101);
    expect(r.body.error.type).toEqual('missing_access_key');
  });
});

import supertest from 'supertest';

import { urls } from '../config/index';

const Mailboxlayer = function Mailboxlayer() {
  this.checkEmail = async function checkEmail(email, token) {
    const r = supertest(urls.mailbowlayer).get('/check').query({ email, access_key: token });
    return r;
  };
};

export { Mailboxlayer };
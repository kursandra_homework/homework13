import { Mailboxlayer } from './services/index';

const apiProvider = () => ({
  mailboxlayer: () => new Mailboxlayer(),
});

export { apiProvider };
const email = {
  positiveEmail: 'kursandra@gmail.com',
};
const diffEmails = [
  ['_sandra_@inbox.ru', true],
  ['123@gmail.com', true],
  ['strangeEmail_1243567@sd.com', true],
  ['.com', false],
  ['1@2', false],
  ['iuewiuewuw', false],
];

export { email, diffEmails };